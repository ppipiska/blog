# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Blog::Application.config.secret_key_base = 'b2e5c0ca61b917bf2ee2a60f693bb55ecb7edcdc0c43e29a26e0c16d94e2e8b2d48cf5f4e1474be026020cb58803495486360a98121d1a85f78592c9eef9eb18'
